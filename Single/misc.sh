#VirtualBox Linux kernel driver aktivieren
sudo modprobe vboxdrv

#Spaces auf allen Monitoren aktivieren
gsettings set org.gnome.mutter workspaces-only-on-primary false

#Numblock direkt bei Boot aktivieren
#sudo su gdm -s /bin/bash
gsettings set org.gnome.settings-daemon.peripherals.keyboard numlock-state 'on'

#Exiftools
pip update
sudo apt-get install libimage-exiftool-perl perl-doc
pip install pyexifinfo

